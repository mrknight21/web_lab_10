/**
 * Created by User on 24/04/2017.
 */

$(function(){

    $(".dolphin").hide();
    $("#dolphin1").show();

    $( "input[name='radio']" ).on( "change", function() {
        console.log($(this).checked);
        if ($(this).is(':checked'))
        {
            var Src = this.id;
            var picSrc = Src.substring(6);
            if (picSrc != "background6"){
            $('#background').attr("src","../images/ex4/"+picSrc+".jpg");
            console.log("work2");
            }
            else{
                $('#background').attr("src","../images/ex4/"+picSrc+".gif");
            }
        }
    });
    //Above are the script for the backgound checkbox//


    $( "input[type='checkbox']" ).on( "change", function() {
        var Src = this.id;
        var picSrc = Src.substring(6);
        if ($(this).is(':checked'))
        {
            $("#"+picSrc).show();
        }
        else{
            $("#"+picSrc).hide();
        }
    });
    //Above are the script for the dolphins checkbox//

    var originPointD = 100;
    var preSize = 100;
    $(".slider").css("margin","10px").css("width","300px");
    $("#size-control").slider({
        value: 100,
        min: 1,
        max: 200,
        step: 10,
        slide: function (event, ui) {
            var origin = preSize/originPointD;
            var size = ui.value;
            preSize = size;
            $(".dolphin").each(function () {
                var origHeight = $(this).height();
                var newHeight = (origHeight*size/100/origin)+"px";
                $(this).height(newHeight);
                console.log($(this).height());
            })
    }
    });




    var preVerchange = 0;
    $("#vertical-control").slider({
        value: 0,
        min: -200,
        max: 200,
        step: 10,
        slide: function (event, ui) {
            var distanceVer = ui.value;
            var verChange = distanceVer-preVerchange+"px";
            preVerchange = distanceVer;
            $(".dolphin").each(function () {
                $(this).animate({top: '-='+verChange},100);
            });
        }
    });



    var preHorChange = 0;
    $("#horizontal-control").slider({
        value: 0,
        min: -200,
        max: 200,
        step: 10,
        slide: function (event, ui) {
            var distanceHor = ui.value;
            var horChange = distanceHor-preHorChange+"px";
            preHorChange = distanceHor;
            console.log(horChange);
            $(".dolphin").each(function () {
                $(this).animate({left: '+='+horChange},100);
            });
        }
    });


});